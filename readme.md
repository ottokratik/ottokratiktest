Headline ([example](https://gitlab.com/ottokratik/ottokratiktest/uploads/ef83d2fc3a63e031624cb75ebf74fcfc/FileHex.exe))
========

This is a **bolder** test.

## Section 1

*   Item 1
*   Item 2
* Item 3


## Section 2

*   Item 1
*   Item 2


## Section 3

Some more text


## Section 4

![alt text](https://gitlab.com/ottokratik/ottokratiktest/uploads/3efa1f3a5e0bfba466b684ac165e2127/screenshot01.png)



An [example](https://bitbucket.org/ottokratik/ottokratiktest/downloads/FileHex.exe "Title")

https://bitbucket.org/ottokratik/ottokratiktest/downloads/FileHex.exe


A list:

*   Item 1
*   Item 2
* Item 3